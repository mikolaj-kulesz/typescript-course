"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Shorten a URL string to a given length
 * @param originalUrl URL to shorten
 * @param length maximum allowed length
 */
function shortUrl(originalUrl, length) {
    if (originalUrl === void 0) { originalUrl = ''; }
    if (length === void 0) { length = 50; }
    var chunk_l = length / 2;
    var url = originalUrl.replace('http://', '').replace('https://', '');
    if (url.length <= length) {
        return url;
    }
    var start_chunk = shortString(url, chunk_l, false);
    var end_chunk = shortString(url, chunk_l, true);
    return start_chunk + ".." + end_chunk;
}
exports.shortUrl = shortUrl;
function shortString(str, l, rev) {
    var stop_chars = [' ', '/', '&'];
    var acceptable_shortness = l * 0.80; // When to start looking for stop characters
    var reverse = typeof rev !== 'undefined' ? rev : false;
    var s = reverse ? str.split('').reverse().join('') : str;
    var short_s = '';
    for (var i = 0; i < l - 1; i++) {
        short_s += s[i];
        if (i >= acceptable_shortness && stop_chars.indexOf(s[i]) >= 0) {
            break;
        }
    }
    if (reverse) {
        return short_s.split('').reverse().join('');
    }
    return short_s;
}
//# sourceMappingURL=string.js.map