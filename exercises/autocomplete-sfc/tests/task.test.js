"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var place_search_result_1 = require("../src/place-search-result");
var renderer = require("react-test-renderer");
var hasStudentSolution = true;
hasStudentSolution =
    JSON.stringify(renderer.create(React.createElement(place_search_result_1.PlaceSearchResult, null)).toTree()) !==
        JSON.stringify({
            nodeType: 'component',
            props: {},
            instance: null,
            rendered: { nodeType: 'host', type: 'div', props: {}, instance: null, rendered: null }
        });
if (hasStudentSolution) {
    it('renders correctly if place is missing a website', function () {
        var pd = {
            id: 'e3290ca5b95282e14c0b14f98666ff7886efa359',
            name: 'Donut Hut',
            rating: 1.1,
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png',
            vicinity: '31 Little Canada Road East, Little Canada',
            url: 'https://maps.google.com/?cid=12061977042674382991'
        };
        var tree = renderer.create(React.createElement(place_search_result_1.PlaceSearchResult, __assign({}, pd))).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('renders correctly if place has a website', function () {
        var pd = {
            id: 'e3290ca5b95282e14c0b14f98666ff7886efa359',
            name: 'Donut Hut',
            rating: 1.1,
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png',
            vicinity: '31 Little Canada Road East, Little Canada',
            url: 'https://maps.google.com/?cid=12061977042674382991',
            website: 'http://example.com'
        };
        var tree = renderer.create(React.createElement(place_search_result_1.PlaceSearchResult, __assign({}, pd))).toJSON();
        expect(tree).toMatchSnapshot();
    });
}
else {
    describe('Instructions', function () {
        test('Please implement the "autocomplete-sfc/src/place-search-result.tsx" component', function () {
            expect(true).toBeTruthy();
        });
    });
}
//# sourceMappingURL=task.test.js.map