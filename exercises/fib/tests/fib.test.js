"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fib = require("../src/fib");
if (Object.keys(fib).length > 0) {
    describe('fib.js exports are correct', function () {
        test('getFibSequence export is found', function () {
            expect(typeof fib.getFibSequence).toBe('function');
        });
        test('only one thing exported from the module', function () {
            expect(Object.keys(fib).length).toBe(1);
        });
    });
    describe('fib.js#getFibSequence basic cases', function () {
        test('getFibSequence() returns an iterator', function () {
            expect(typeof fib.getFibSequence()).toBe('object');
            expect(Object.keys(fib.getFibSequence())).toMatchObject(['next', 'throw', 'return']);
        });
        test('getFibSequence() first number -> 1', function () {
            var it = fib.getFibSequence();
            expect(it.next().value).toBe(1);
        });
        test('getFibSequence() -> 2nd number -> 1', function () {
            var it = fib.getFibSequence();
            expect(it.next().value).toBe(1);
            expect(it.next().value).toBe(1);
        });
        test('getFibSequence() -> 3rd number -> 2', function () {
            var it = fib.getFibSequence();
            expect(it.next().value).toBe(1);
            expect(it.next().value).toBe(1);
            expect(it.next().value).toBe(2);
        });
        test('getFibSequence() -> 4th number -> 3', function () {
            var it = fib.getFibSequence();
            expect(it.next().value).toBe(1);
            expect(it.next().value).toBe(1);
            expect(it.next().value).toBe(2);
            expect(it.next().value).toBe(3);
        });
        test('getFibSequence() -> 5th number -> 5', function () {
            var it = fib.getFibSequence();
            expect(it.next().value).toBe(1);
            expect(it.next().value).toBe(1);
            expect(it.next().value).toBe(2);
            expect(it.next().value).toBe(3);
            expect(it.next().value).toBe(5);
        });
    });
}
else {
    describe('Instructions', function () {
        test('Please uncomment the getFibSequence generator function in in fib/src/fib.ts', function () {
            expect(true).toBeTruthy();
        });
    });
}
//# sourceMappingURL=fib.test.js.map