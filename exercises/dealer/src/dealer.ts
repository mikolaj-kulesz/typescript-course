/**
 * Shuffle an array in place
 * @param a Array to shuffle
 */
function shuffleArray(a: any[]) {
    // Iterate over the array
    for (let i = a.length; i; i--) {
        // Get next index
        let j = Math.floor(Math.random() * i);
        // Swap positions
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
}

export enum Suit {
    Clubs, Diamonds, Hearts, Spades
};

export enum CardNumber {
    Ase, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten,
    Jack, Queen, King
};

type Card = [Suit, CardNumber];

export class Dealer {
    cards: Card[] = [];
    constructor(){
        this.cards = this.createDeck()
        shuffleArray(this.cards)
    }
    createDeck(): Card[]{
        let cards: Card[] =[];
        for (let s in Suit) {
            for (let c in CardNumber) {
                if (!isNaN(Number(s)) && !isNaN(Number(c))) { //only numbers not strings
                    cards.push([parseInt(s,10), parseInt(c,10)])
                }
            }
        }
        return cards;
    }
    // deals 5 cards [Suit(0-3), Number(0-12)]
    dealHand(n: number): Card[] {
        if(n < 0) throw "No negative number";
        if(n >  this.getLength()) throw "Asking for too many cards";
        return this.cards.splice(this.getLength() - n, n);
    }

    //number of cards left in the deck
    getLength(): number {
        return this.cards.length;
    }

    //read: "Seven of Spades"
    readCard(c: Card): string {
        let [suit, number] = c;
        return `${CardNumber[number]} of ${Suit[suit]}`;
    }
}

/*let d = new Dealer()
console.log('-->', d.createDeck())
console.log('-->', d.getLength())*/

