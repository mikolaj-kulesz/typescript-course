"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Shuffle an array in place
 * @param a Array to shuffle
 */
function shuffleArray(a) {
    var _a;
    // Iterate over the array
    for (var i = a.length; i; i--) {
        // Get next index
        var j = Math.floor(Math.random() * i);
        // Swap positions
        _a = [a[j], a[i - 1]], a[i - 1] = _a[0], a[j] = _a[1];
    }
}
var Suit;
(function (Suit) {
    Suit[Suit["Clubs"] = 0] = "Clubs";
    Suit[Suit["Diamonds"] = 1] = "Diamonds";
    Suit[Suit["Hearts"] = 2] = "Hearts";
    Suit[Suit["Spades"] = 3] = "Spades";
})(Suit = exports.Suit || (exports.Suit = {}));
;
var CardNumber;
(function (CardNumber) {
    CardNumber[CardNumber["Ase"] = 0] = "Ase";
    CardNumber[CardNumber["Two"] = 1] = "Two";
    CardNumber[CardNumber["Three"] = 2] = "Three";
    CardNumber[CardNumber["Four"] = 3] = "Four";
    CardNumber[CardNumber["Five"] = 4] = "Five";
    CardNumber[CardNumber["Six"] = 5] = "Six";
    CardNumber[CardNumber["Seven"] = 6] = "Seven";
    CardNumber[CardNumber["Eight"] = 7] = "Eight";
    CardNumber[CardNumber["Nine"] = 8] = "Nine";
    CardNumber[CardNumber["Ten"] = 9] = "Ten";
    CardNumber[CardNumber["Jack"] = 10] = "Jack";
    CardNumber[CardNumber["Queen"] = 11] = "Queen";
    CardNumber[CardNumber["King"] = 12] = "King";
})(CardNumber = exports.CardNumber || (exports.CardNumber = {}));
;
var Dealer = /** @class */ (function () {
    function Dealer() {
        this.cards = [];
        this.cards = this.createDeck();
        shuffleArray(this.cards);
    }
    Dealer.prototype.createDeck = function () {
        var cards = [];
        for (var s in Suit) {
            for (var c in CardNumber) {
                if (!isNaN(Number(s)) && !isNaN(Number(c))) { //only numbers not strings
                    cards.push([parseInt(s, 10), parseInt(c, 10)]);
                }
            }
        }
        return cards;
    };
    // deals 5 cards [Suit(0-3), Number(0-12)]
    Dealer.prototype.dealHand = function (n) {
        if (n < 0)
            throw "No negative number";
        if (n > this.getLength())
            throw "Asking for too many cards";
        return this.cards.splice(this.getLength() - n, n);
    };
    //number of cards left in the deck
    Dealer.prototype.getLength = function () {
        return this.cards.length;
    };
    //read: "Seven of Spades"
    Dealer.prototype.readCard = function (c) {
        var suit = c[0], number = c[1];
        return CardNumber[number] + " of " + Suit[suit];
    };
    return Dealer;
}());
exports.Dealer = Dealer;
/*let d = new Dealer()
console.log('-->', d.createDeck())
console.log('-->', d.getLength())*/
//# sourceMappingURL=dealer.js.map