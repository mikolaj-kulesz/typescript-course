"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// hexToRgb
function hexToRgb(hex) {
    if (hex.length === 3) {
        return hexToRgb([0, 1, 2]
            .map(function (from) { return hex.substr(from, 1).repeat(2); })
            .join(''));
    }
    var _a = [0, 2, 4].map(function (from) { return toRgb(hex, from, 2); }), r = _a[0], g = _a[1], b = _a[2];
    return { r: r, g: g, b: b };
}
exports.hexToRgb = hexToRgb;
// rgbToHex
function rgbToHex(r, g, b) {
    return [r, g, b]
        .map(function (item) { return toHex(item); })
        .join('');
}
exports.rgbToHex = rgbToHex;
// helpers
function toRgb(hex, from, length) {
    return parseInt(hex.substr(from, length), 16);
}
function toHex(n) {
    var m;
    m = Math.max(0, Math.min(n, 255));
    m = m.toString(16);
    m = (m.length === 1) ? "0" + m : "" + m;
    return m;
}
//# sourceMappingURL=color-utils.js.map