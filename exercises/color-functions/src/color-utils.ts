// hexToRgb
export function hexToRgb(hex: string):  {r: number, g: number, b: number} {
    if(hex.length === 3)  {
        return hexToRgb(
            [0,1,2]
                .map(from => hex.substr(from, 1).repeat(2))
                .join('')
        )
    }
    let [r, g, b] = [0,2,4].map(from => toRgb(hex, from, 2))
    return {r, g, b}
}

// rgbToHex
export function rgbToHex(r: number, g: number, b: number): string {
    return [r,g,b]
        .map(item => toHex(item))
        .join('');
}

// helpers
function toRgb(hex: string, from: number, length: number): number {
    return parseInt(hex.substr(from, length), 16)
}
function toHex(n: number): string {
    let m;
    m = Math.max(0, Math.min(n, 255));
    m = m.toString(16);
    m = (m.length === 1) ? `0${m}`: `${m}`;
    return m;
}