"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var stack_1 = require("../src/stack");
var exp = expect;
if (stack_1.Stack) {
    test('Stack is available as a named export from ./src/stack.ts', function () {
        exp(stack_1.Stack).toBeDefined();
    });
    test('new stack has size of 0', function () {
        var l = new stack_1.Stack();
        exp(l.length()).toBe(0);
    });
    test('pushing an item to the stack increases its size by 1', function () {
        var l = new stack_1.Stack();
        l.push('abc');
        exp(l.length()).toBe(1);
    });
    test('pushing a few items to the stack (one-by-one) increases its size appropriately', function () {
        var l = new stack_1.Stack();
        l.push('abc');
        l.push('abc');
        l.push('abc');
        l.push('abc');
        exp(l.length()).toBe(4);
    });
    test('pushing a few items to the stack (at once) increases its size appropriately', function () {
        var l = new stack_1.Stack();
        l.push(['abc', 'def', 'ghi', 'jkl']);
        exp(l.length()).toBe(4);
    });
    test('pushing a few items to the stack (at once) increases its size appropriately', function () {
        var l = new stack_1.Stack();
        l.push(['abc', 'def', 'ghi', 'jkl']);
        exp(l.length()).toBe(4);
    });
    test('last items pushed on are the first to pop off', function () {
        var l = new stack_1.Stack();
        l.push(['abc', 'def', 'ghi', 'jkl']);
        var last = l.pop();
        exp(l.length()).toBe(3);
        exp(last).toBe('jkl');
    });
    test('pop() returns undefined if the list is empty', function () {
        var l = new stack_1.Stack();
        var last = l.pop();
        exp(last).toBeUndefined();
    });
}
else {
    describe('Instructions', function () {
        test('Please uncomment the Stack class in stack/src/stack.ts', function () {
            expect(true).toBeTruthy();
        });
    });
}
//# sourceMappingURL=stack.test.js.map