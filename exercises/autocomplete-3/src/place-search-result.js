"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var string_1 = require("./utils/string");
exports.PlaceSearchResult = function (placeInfo) {
    var url = placeInfo.website ? React.createElement("a", { href: placeInfo.website, target: "_blank" }, string_1.shortUrl(placeInfo.website, 20)) : '';
    return (React.createElement("li", { key: placeInfo.url, className: "search-result" },
        React.createElement("img", { className: "icon", src: placeInfo.icon }),
        React.createElement("h3", null, placeInfo.name),
        React.createElement("p", null,
            React.createElement("a", { href: placeInfo.url, target: "_blank" }, placeInfo.vicinity),
            ' ',
            "-",
            ' ',
            url)));
};
//# sourceMappingURL=place-search-result.js.map