"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var PlaceSearchContainer = /** @class */ (function (_super) {
    __extends(PlaceSearchContainer, _super);
    function PlaceSearchContainer() {
        var _this = _super.call(this) || this;
        _this.state = {};
        // Event handler for changes to search term
        _this.beginSearch = _this.beginSearch.bind(_this);
        return _this;
    }
    /**
     * Event handler for changes to the serch term
     *
     * @param {InputEvent} evt from the search field
     *
     * @memberof PlaceSearch
     * @return {undefined}
     */
    PlaceSearchContainer.prototype.beginSearch = function (term) {
        // Initiate a search using the ./autocomplete.ts module
        // When the promise it returns resolves, update your state accordingly
    };
    /**
     * Render the html for this component
     *
     * @param {JSX.Element} elem element
     * @param {Object} container component state
     * @returns {undefined}
     *
     * @memberof PlaceSearch
     */
    PlaceSearchContainer.prototype.render = function () {
        return (React.createElement("p", null, "Replace this with a PlaceSearchResultList")
        // <PlaceSearchResultList />
        );
    };
    return PlaceSearchContainer;
}(React.Component));
exports.PlaceSearchContainer = PlaceSearchContainer;
//# sourceMappingURL=place-search-container.js.map