"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var place_search_container_1 = require("../src/place-search-container");
var renderer = require("react-test-renderer");
var hasStudentSolution = true;
hasStudentSolution =
    JSON.stringify(renderer.create(React.createElement(place_search_container_1.PlaceSearchContainer, null)).toJSON()) !==
        JSON.stringify({ type: 'p', props: {}, children: ['Replace this with a PlaceSearchResultList'] });
if (hasStudentSolution) {
    it('PlaceSearchContainer renders correctly', function () {
        var tree = renderer.create(React.createElement(place_search_container_1.PlaceSearchContainer, null)).toJSON();
        expect(tree).toMatchSnapshot();
    });
}
else {
    describe('Instructions', function () {
        test('Please implement the "autocomplete-3/src/place-search-container.tsx" component', function () {
            expect(true).toBeTruthy();
        });
    });
}
//# sourceMappingURL=autocomplete-3.test.js.map