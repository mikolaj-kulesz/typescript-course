"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var place_search_result_list_1 = require("../src/place-search-result-list");
var renderer = require("react-test-renderer");
var hasStudentSolution = true;
hasStudentSolution =
    JSON.stringify(renderer.create(React.createElement(place_search_result_list_1.PlaceSearchResultList, null)).toJSON()) !==
        JSON.stringify({ type: 'pre', props: {}, children: ['{"0":{},"1":{}}'] });
if (hasStudentSolution) {
    it('PlaceSearchResultList renders correctly for "in progress" state', function () {
        var tree = renderer
            .create(React.createElement(place_search_result_list_1.PlaceSearchResultList, { inProgress: true, term: 'xylophone', results: [] }))
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('PlaceSearchResultList renders correctly for "not yet started" state', function () {
        var tree = renderer
            .create(React.createElement(place_search_result_list_1.PlaceSearchResultList, { inProgress: false, term: '', results: [] }))
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('PlaceSearchResultList renders correctly for populated set of results', function () {
        var genPd = function (x) { return ({
            id: "" + x,
            name: 'Donut Hut',
            rating: 1.1,
            icon: 'https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png',
            vicinity: '31 Little Canada Road East, Little Canada',
            url: "https://maps.google.com/?cid=12061977042674382991" + x
        }); };
        var tree = renderer
            .create(React.createElement(place_search_result_list_1.PlaceSearchResultList, { inProgress: false, term: 'xylophone', results: [genPd(1), genPd(2), genPd(3)] }))
            .toJSON();
        expect(tree).toMatchSnapshot();
    });
}
else {
    describe('Instructions', function () {
        test('Please implement the "autocomplete-2/src/place-search-result-list.tsx" component', function () {
            expect(true).toBeTruthy();
        });
    });
}
//# sourceMappingURL=autocomplete-2.test.js.map