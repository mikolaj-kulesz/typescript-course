"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var AccountManager = /** @class */ (function () {
    function AccountManager() {
        this.users = [];
    }
    /**
     * Create a new user account
     * @param email email address
     * @param password the user's password
     * @return the new user account. An admin must activate it using activateNewUser
     * @see this.activateNewUser
     */
    AccountManager.prototype.register = function (email, password) {
        if (!email)
            throw 'Must provide an email';
        if (!password)
            throw 'Must provide a password';
        var user = { email: email, password: password };
        this.users.push(user);
        return user;
    };
    /**
     * Activate a new user account
     * @param approver The admin who's approving this new user
     * @param userToApprove Newly-registered user, who is to be activated
     * @return the updated user object, now activated
     */
    AccountManager.prototype.activateNewUser = function (approver, userToApprove) {
        if (!approver.adminSince)
            throw "Approver is not an admin!";
        return __assign({}, userToApprove, { isActive: true });
    };
    /**
     * Promote a normal user to admin
     * @param existingAdmin admin who is promoting another user
     * @param user an active user who you're making an admin
     * @return the updated user object, now can also be regarded as an admin
     */
    AccountManager.prototype.promoteToAdmin = function (existingAdmin, user) {
        if (!existingAdmin.adminSince)
            throw "Not an admin!";
        if (user.isActive !== true)
            throw "User must be active in order to be promoted to admin!";
        return __assign({}, user, { adminSince: new Date() });
    };
    return AccountManager;
}());
exports.AccountManager = AccountManager;
//# sourceMappingURL=account-manager.js.map