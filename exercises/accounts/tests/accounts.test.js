"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var account_manager_1 = require("../src/account-manager");
var exp = expect;
test('AccountManager is available as a named export from ./src/account-manager.ts', function () {
    exp(account_manager_1.AccountManager).toBeDefined();
});
test('Registering a new user', function () {
    var am = new account_manager_1.AccountManager();
    var newUser = am.register('mike@example.com', '123456Seven');
    exp(newUser).toBeDefined();
    exp(newUser.email).toEqual('mike@example.com');
    exp(newUser.password).toEqual('123456Seven');
});
test('Activating a new user', function () {
    var am = new account_manager_1.AccountManager();
    var admin = { email: 'lisa@example.com', password: '23456Seven', isActive: true, adminSince: new Date() };
    var newUser = am.register('mike@example.com', '123456Seven');
    var activatedUser = am.activateNewUser(admin, newUser);
    exp(activatedUser).toBeDefined();
    exp(activatedUser.email).toEqual('mike@example.com');
    exp(activatedUser.password).toEqual('123456Seven');
    exp(activatedUser.isActive).toBe(true);
});
test('Promoting an activated user to admin', function () {
    var am = new account_manager_1.AccountManager();
    var admin = { email: 'lisa@example.com', password: '23456Seven', isActive: true, adminSince: new Date() };
    var newUser = am.register('mike@example.com', '123456Seven');
    var activatedUser = am.activateNewUser(admin, newUser);
    var newAdmin = am.promoteToAdmin(admin, activatedUser);
    exp(newAdmin).toBeDefined();
    exp(newAdmin.email).toEqual('mike@example.com');
    exp(newAdmin.password).toEqual('123456Seven');
    exp(newAdmin.isActive).toBe(true);
    exp(typeof newAdmin.adminSince).toBe('object');
    exp(typeof newAdmin.adminSince.toISOString).toBe('function');
});
//# sourceMappingURL=accounts.test.js.map