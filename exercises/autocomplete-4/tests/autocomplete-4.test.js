"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var app_1 = require("../src/app");
var renderer = require("react-test-renderer");
it('PlaceSearchContainer renders correctly', function () {
    var tree = renderer.create(React.createElement(app_1.App, null)).toJSON();
    expect(tree).toMatchSnapshot();
});
//# sourceMappingURL=autocomplete-4.test.js.map