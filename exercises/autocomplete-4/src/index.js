"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_dom_1 = require("react-dom");
// import { PlaceSearch } from './PlaceSearch';
var app_1 = require("./app");
/**
 * Render the <PlaceSearch> component into the #root element
 */
var root = document.getElementById('root');
react_dom_1.render(React.createElement(app_1.App, null), root);
//# sourceMappingURL=index.js.map