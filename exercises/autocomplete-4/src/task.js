"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Given a generator function that yields one or more
 * promises, chain them together in sequence
 *
 * @param {any} genFn generator function that yields one or more promises
 * @return {undefined}
 */
function task(genFn) {
    var p = new Promise(function (resolve) {
        var iterator = genFn(); // Get the iterator
        // TODO: implement your solution here
    });
    return p;
}
exports.task = task;
//# sourceMappingURL=task.js.map