"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var place_search_result_1 = require("./place-search-result");
exports.PlaceSearchResultList = function (_a) {
    var onSearchTermChanged = _a.onSearchTermChanged, term = _a.term, inProgress = _a.inProgress, results = _a.results;
    /**
     * There are a bunch of different things that we could end up
     * displaying as feedback to the user. Below we handle each of
     * four cases independently, taking care to always have searchResults
     * be an array.
     */
    var searchResults = null;
    if (term.length === 0) {
        // No search yet
        searchResults = [
            React.createElement("li", { key: 'goahead' }, "Type something in above to search")
        ];
    }
    else if (inProgress) {
        // Search in progress
        searchResults = [
            React.createElement("li", { key: 'searching', className: "blue" },
                "Searching for ",
                term,
                "...")
        ];
    }
    else if (results.length === 0) {
        // No results found
        searchResults = [
            React.createElement("li", { key: 'noresults', className: "red" },
                "Sorry! No results for ",
                term,
                ".")
        ];
    }
    else {
        // Search complete
        var i = 0;
        searchResults = results.map(function (r) { return (React.createElement(place_search_result_1.PlaceSearchResult, __assign({ key: r.url }, r))); });
    }
    return (React.createElement("div", null,
        React.createElement("h2", null, "Search for a place"),
        React.createElement("input", { type: "search", placeholder: "Search", onChange: function (e) { return onSearchTermChanged ? onSearchTermChanged(e.target.value) : function () { }; } }),
        React.createElement("ul", { className: "results" }, searchResults)));
};
//# sourceMappingURL=place-search-result-list.js.map