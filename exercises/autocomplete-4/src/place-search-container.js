"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var autocomplete_1 = require("./autocomplete");
var place_search_result_list_1 = require("./place-search-result-list");
var PlaceSearchContainer = /** @class */ (function (_super) {
    __extends(PlaceSearchContainer, _super);
    function PlaceSearchContainer() {
        var _this = _super.call(this) || this;
        _this.state = {
            term: '',
            results: [],
            existingSearch: undefined // Most recent search (a promise that resolves to an array)
        };
        // Event handler for changes to search term
        _this.beginSearch = _this.beginSearch.bind(_this);
        return _this;
    }
    /**
     * Event handler for changes to the serch term
     *
     * @param {InputEvent} evt from the search field
     *
     * @memberof PlaceSearch
     * @return {undefined}
     */
    PlaceSearchContainer.prototype.beginSearch = function (term) {
        var _this = this;
        // Kick off the new search, with the new search term
        var p = autocomplete_1.autocomplete(term);
        // Update the existingSearch state, so our component re-renders
        //   (probably to update the "Searching for <term>..." message)
        this.setState({ term: term, existingSearch: p, results: [] });
        // Attach a promise handler to the search.
        //  THIS WILL ONLY BE INVOKED IF THE SEARCH RUNS TO COMPLETION
        p.then(function (results) {
            // When the search completes, update the "results" state, triggering a re-render
            _this.setState({ results: results, existingSearch: undefined });
        });
    };
    /**
     * Render the html for this component
     *
     * @param {JSX.Element} elem element
     * @param {Object} container component state
     * @returns {undefined}
     *
     * @memberof PlaceSearch
     */
    PlaceSearchContainer.prototype.render = function () {
        var _this = this;
        return (React.createElement(place_search_result_list_1.PlaceSearchResultList, { results: this.state.results, inProgress: !!this.state.existingSearch && this.state.results.length === 0, term: this.state.term, onSearchTermChanged: function (s) { return _this.beginSearch(s); } }));
    };
    return PlaceSearchContainer;
}(React.Component));
exports.PlaceSearchContainer = PlaceSearchContainer;
//# sourceMappingURL=place-search-container.js.map