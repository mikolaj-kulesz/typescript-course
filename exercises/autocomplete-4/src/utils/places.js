"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Search the google places API, for a particular term
 *
 * @export
 * @param {string} input Search term
 * @returns {Promise} promise that resolves to search results
 */
function fetchPlaceSummaries(input) {
    return fetch("http://localhost:3000/maps/api/place/autocomplete/json?types=establishment&input=" + input)
        .then(function (response) { return response.json(); })
        .then(function (jsonData) {
        return jsonData.predictions;
    });
}
exports.fetchPlaceSummaries = fetchPlaceSummaries;
function fetchPlaceDetails(placeids) {
    return Promise.all(placeids.map(function (placeid) {
        return fetch("http://localhost:3000/maps/api/place/details/json?placeid=" + placeid)
            .then(function (response) { return response.json(); })
            .then(function (jsonData) { return jsonData.result; });
    }));
}
exports.fetchPlaceDetails = fetchPlaceDetails;
//# sourceMappingURL=places.js.map