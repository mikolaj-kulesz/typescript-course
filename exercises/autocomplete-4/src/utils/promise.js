"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Check to see whether a value is a promise
 *
 * @param {any} x value to check
 * @returns {boolean} true if the value is found to be a promise
 */
function isPromise(x) {
    return x && typeof x.then === 'function';
}
exports.isPromise = isPromise;
/**
 * Returns a promise that will resolve in a particular amount of time
 *
 * @param {number} time time to wait (in ms)
 * @returns {Promise} a promise that will resolve after time
 */
function wait(time) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve();
        }, time);
    });
}
exports.wait = wait;
//# sourceMappingURL=promise.js.map