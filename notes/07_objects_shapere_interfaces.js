"use strict";
// Type '{ make: string; model: string; year: number; color: { r: number; g: number; b: number; }; }' is not assignable to type '{ make: string; model: string; year: number; }'.
// Object literal may only specify known properties, and 'color' does not exist in type '{ make: string; model: string; year: number; }'.
var myCar;
myCar = {
    make: 'Honda',
    model: 'Accord',
    year: 1992,
    color: { r: 255, g: 0, b: 0 }
};
// Easy way to deal with this: explicitly cast the type of the object to the appropriate type
var myCar2;
myCar2 = {
    make: 'Honda',
    model: 'Accord',
    year: 1992,
    color: { r: 255, g: 0, b: 0 }
};
// or
var myCar3 = {
    make: 'Honda',
    model: 'Accord',
    year: 1992,
    color: { r: 255, g: 0, b: 0 }
};
// My example of casting the object shape
var face;
face = {
    nose: true,
    mouth: true,
    eyes: 2,
    lips: true
};
// Real life example:
// How to avoid repetitive like this:
var someCar = {
    make: 'Honda',
    model: 'Accord',
    year: 1992
};
var lisasCar = {
    make: 'Ford',
    model: 'Monster Truck',
    year: 2016
};
function carCageMatch(a, b) { }
;
var someCar2 = {
    make: 'Honda',
    model: 'Accord',
    year: 1992
};
var lisasCar2 = {
    make: 'Ford',
    model: 'Monster Truck',
    year: 2016
};
function carTest(car1, car2) {
    return car1;
}
;
var lisasCar = {
    make: 'Ford',
    model: 'Monster Truck',
    year: 2016,
    color: '#fff'
};
//# sourceMappingURL=07_objects_shapere_interfaces.js.map