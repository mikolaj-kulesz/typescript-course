"use strict";
// Arrays
// By default, arrays work same as in JavaScript
// Adding a type constraint helps us keep contents consistent
// When initializing class properties with empty arrays, provide a type!
// Example:
var a = [];
a.push(5);
a.push("not a number");
// Or
var nums = [1, 2, 3];
// Class:
var ShoppingCart = /** @class */ (function () {
    function ShoppingCart() {
        this.items = [];
        this.items.push(5); //Argument of type '5' is not assignable to parameter of type 'never
    }
    return ShoppingCart;
}());
// Fix:
var ShoppingCart = /** @class */ (function () {
    function ShoppingCart() {
        this.items = [];
        this.items.push(5); //Argument of type '5' is not assignable to parameter of type 'never
    }
    return ShoppingCart;
}());
// Tuples
// Arrays of fixed length
// Typically represent values that are related in some way
// Consumers need to know about order
// Shines with destructured assignment
var dependency;
dependency = ['react', 16];
var dependencies = [];
dependencies.push(dependency);
dependencies.push([
    'webpack', 3
]);
dependencies.push([
    'typescript', '2.5'
]);
/*
    Argument of type '[string, string]' is not assignable to parameter of type '[string, number]'.
    Type 'string' is not assignable to type 'number'.
*/
//# sourceMappingURL=12_arrays_tuples.js.map