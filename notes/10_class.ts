// Classes
// -> Shape defined up front, like Interfaces
// -> Constructor for creating new instances
// -> Make sure to add type annotations properties AND function arguments

class Car {
    make: string
    model: string
    year: number
    constructor(make: string,
                model: string,
                year: number) {
        this.make = make;
        this.model = model;
        this.year = year;
    }
    startEngine() {
        return 'VROOOOOM!';
    }
}

let myCar = new Car('Honda', 'Accord', 2017);