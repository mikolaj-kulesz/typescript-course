"use strict";
// Classes
// -> Shape defined up front, like Interfaces
// -> Constructor for creating new instances
// -> Make sure to add type annotations properties AND function arguments
var Car = /** @class */ (function () {
    function Car(make, model, year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }
    Car.prototype.startEngine = function () {
        return 'VROOOOOM!';
    };
    return Car;
}());
var myCar = new Car('Honda', 'Accord', 2017);
//# sourceMappingURL=10_class.js.map