"use strict";
// Used to define a type consisting of ordered members
// Each has a name and a value
// Often we don't care about the value
// ...beyond an equality check
// Get number of members via:
// Example:
var AcctType;
(function (AcctType) {
    AcctType[AcctType["Checking"] = 0] = "Checking";
    AcctType[AcctType["Savings"] = 1] = "Savings";
    AcctType[AcctType["MoneyMarket"] = 2] = "MoneyMarket";
})(AcctType || (AcctType = {}));
;
var account = [
    9142.14, AcctType.Checking
];
// Example 2:
var Suit;
(function (Suit) {
    Suit[Suit["Club"] = 0] = "Club";
    Suit[Suit["Diamond"] = 1] = "Diamond";
    Suit[Suit["Heart"] = 2] = "Heart";
    Suit[Suit["Spade"] = 3] = "Spade";
})(Suit || (Suit = {}));
// converts to this in JS
var Suit;
(function (Suit) {
    Suit["Club"] = 0;
    Suit[0] = "Club";
    Suit["Diamond"] = 1;
    Suit[1] = "Diamond";
    Suit["Heart"] = 2;
    Suit[2] = "Heart";
    Suit["Spade"] = 3;
    Suit[3] = "Spade";
})(Suit | (Suit = {}));
// to get the length
Object.keys(Suit).length / 2; // 4
//# sourceMappingURL=11_enums.js.map