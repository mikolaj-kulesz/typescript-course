"use strict";
//Explicit typing: annotations
var age = 28;
var input1 = document.querySelector('input#name_field');
//Explicit typing: casting
var input2 = document.querySelector('input#name_field');
//Explicit typing: function parameters and return
function login(userName, password) {
    //do sth
    return {};
}
;
var name = 'Miki';
function getName(firstLetter) {
    return 'dsadsad';
}
;
//# sourceMappingURL=05_explicit_typing.js.map