// Sometimes an interface isn't the best way to describe a structure
// We can use the type keyword to define a type alias
// You can export types and interfaces, so you can consume them in other modules!

type Color = [number, number, number];

let red: Color = [255, 0, 0];