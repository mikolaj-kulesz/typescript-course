//Explicit typing: annotations
let age: number = 28;
let input1: HTMLElement  = document.querySelector('input#name_field');

//Explicit typing: casting
let input2 = document.querySelector('input#name_field') as HTMLElement;

type User = object
//Explicit typing: function parameters and return
function login(userName: string, password: string) : User {
    //do sth
    return {}
};


let name: string = 'Miki';

function getName( firstLetter: string) : string {
    return 'dsadsad'
};