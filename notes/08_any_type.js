"use strict";
// The any type
// -> Allows for a value of any kind
// -> How every mutable JS value is treated
// -> Useful as you migrate code from JS to TS
// -> Start with making all anys explicit, and then squash as many as possible.
// -> There's also a never type, which is compatible with NOTHING.
//examples
var somethig;
somethig = 100;
somethig = 'Mikolaj';
var age = 34;
var myAge = age;
myAge = '35';
var myName = 'Mikolaj';
var someName = myName;
someName = 700;
function odd(a, b) {
    return a + b;
}
function odd2(a, b) {
    return a + b;
}
//examples
var somethig;
somethig = 100;
somethig = 'Mikolaj';
var age = 34;
var myAge = age;
myAge = '35';
var myName = 'Mikolaj';
var someName = myName;
someName = 700;
function odd(a, b) {
    return a + b;
}
function odd2(a, b) {
    return a + b;
}
var x;
x = 100; // Type '100' is not assignable to type 'never'.
// Nothing is assignable to type 'never'.
//# sourceMappingURL=08_any_type.js.map