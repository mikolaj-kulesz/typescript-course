// VAR
// var declarations are always hoisted (brought up) at the top of the global function or scope
// var - scope function
// var - reasign
// var - hoisted
function foo() {
    console.log(x) // 15
    var x = 15
}

// couse it looks like this
function foo() {
    var x;
    console.log(x) // 15
    var x = 15
}

// the var declarations are not block-scoped
// example:
function foo() {
    var x = 15
    if( x > 10) {
        var y = 21
    }
    console.log(x + y) // 36
}

// LET
// let declarations are no hoisted
// let - scope block
// let - reasign
// let - no hoisted
function foo() {
    console.log(x) // undefined
    let x = 15
}
// the let declarations are block-scoped
// example:
function foo() {
    let x = 15
    if( x > 10) {
        let y = 21
    }
    console.log(x + y) // error - y is not defined
}

// CONST
// const declarations are no hoisted
// const - scope block
// const - no reasign
// const - no hoisted
// when object is const you can add/change properties, you can not reassing whole object
// when array is const you can add items, you can not reassing whole array
function foo() {
    const x = 15
    x = 16 // error
}