// The any type
// -> Allows for a value of any kind
// -> How every mutable JS value is treated
// -> Useful as you migrate code from JS to TS
// -> Start with making all anys explicit, and then squash as many as possible.
// -> There's also a never type, which is compatible with NOTHING.

//examples
let somethig: any;
somethig = 100;
somethig = 'Mikolaj'

let age = 34;
let myAge = age as any;
myAge = '35';

let myName = 'Mikolaj';
let someName = myName as any;
someName = 700;

function odd(a, b): number {
    return a + b;
}
function odd2(a: any, b: any): number {
    return a + b;
}

//examples
let somethig: any;
somethig = 100;
somethig = 'Mikolaj'

let age = 34;
let myAge = age as any;
myAge = '35';

let myName = 'Mikolaj';
let someName = myName as any;
someName = 700;

function odd(a, b): number {
    return a + b;
}
function odd2(a: any, b: any): number {
    return a + b;
}

let x: never
x = 100 // Type '100' is not assignable to type 'never'.
// Nothing is assignable to type 'never'.

