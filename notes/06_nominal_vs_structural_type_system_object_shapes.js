"use strict";
// Nominal type system vs. Structural type system
function validateInputField(input) {
    //do sth
}
// Question: can we regard x as an HTMLInputElement?
validateInputField(x);
// Answers:
// Nominal (JAVA) - is this x instance of class/type named HTMLInputElement
// Structural (TYPESCRIPT) - is x have the same shape of an object ( we check object properties ans it's types)
// What is shape here?
// Example:
function washCar(car) {
    return car;
}
var myCar1 = {
    make: 'Honda',
    model: 'Accord',
    year: 1986,
};
var myCar2 = {
    make: 'Honda',
    model: 'Accord',
};
var myCar3 = {
    make: 'Honda',
    model: 'Accord',
    year: 1986,
    color: 'red',
};
washCar(myCar1); // exact same object that declared
washCar(myCar2); // !year missing
washCar(myCar3); // extanded object also fits
//# sourceMappingURL=06_nominal_vs_structural_type_system_object_shapes.js.map