// Arrays
// By default, arrays work same as in JavaScript
// Adding a type constraint helps us keep contents consistent
// When initializing class properties with empty arrays, provide a type!

// Example:
let a = [];
a.push(5);
a.push("not a number");

// Or
let nums: number[] = [1, 2, 3];

// Class:
class ShoppingCart {
    items = [];
    constructor() {
        this.items.push(5); //Argument of type '5' is not assignable to parameter of type 'never
    }
}

// Fix:
class ShoppingCart {
    items: number[] = [];
    constructor() {
        this.items.push(5); //Argument of type '5' is not assignable to parameter of type 'never
    }
}


// Tuples
// Arrays of fixed length
// Typically represent values that are related in some way
// Consumers need to know about order
// Shines with destructured assignment

let dependency: [string, number];
dependency = ['react', 16];

let dependencies: [string, number][] = [];

dependencies.push(dependency);
dependencies.push([
    'webpack', 3
]);

dependencies.push([
    'typescript', '2.5'
]);
/*
    Argument of type '[string, string]' is not assignable to parameter of type '[string, number]'.
    Type 'string' is not assignable to type 'number'.
*/
