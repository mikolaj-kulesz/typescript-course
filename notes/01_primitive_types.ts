/*
# primitive types - 6 types
 - null
 - undefined
 - string
 - number
 - boolean
 - symbol - ES2015

# meaning
 - primitive types are immutable (for example splitting a string means that we are not mutating this string in place, we are creating new strings )
 - primitive types are not objects , have no methods
 - everything else comes from an object

 var x = "some name"
 var y = new String("some name") --> defining this way, mainly always a mistake
 typeof x --> string
 typeof y --> object

# undefined vs null
In JavaScript, undefined means a variable has been declared but has not yet been assigned a value, such as:

var TestVar;
alert(TestVar); //shows undefined
alert(typeof TestVar); //shows undefined
null is an assignment value. It can be assigned to a variable as a representation of no value:

var TestVar = null;
alert(TestVar); //shows null
alert(typeof TestVar); //shows object
From the preceding examples, it is clear that undefined and null are two distinct types: undefined is a type itself (undefined) while null is an object.

# symbol
Name of the symbol is equal to its value

Główną cechą symboli jest to, że każdorazowo przyjmują one całkowicie nową wartość, którą określa dany symbol.
Nawet jeżeli przekażemy im taki sam tekst opisowy, każdy nowo utworzony symbol ma nową wartość, inną od innych symboli:

console.log(Symbol("foo") === Symbol("foo"))
--> false bo oba symbole mają podobny tekst opisowy, ale ich "wartości" są całkowicie unikatowe
 */