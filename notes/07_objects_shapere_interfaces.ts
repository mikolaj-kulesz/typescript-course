// Type '{ make: string; model: string; year: number; color: { r: number; g: number; b: number; }; }' is not assignable to type '{ make: string; model: string; year: number; }'.
// Object literal may only specify known properties, and 'color' does not exist in type '{ make: string; model: string; year: number; }'.
let myCar: { make: string, model: string, year: number };
myCar = {
    make: 'Honda',
    model: 'Accord',
    year: 1992,
    color: {r: 255, g: 0, b: 0}
}

// Easy way to deal with this: explicitly cast the type of the object to the appropriate type
let myCar2: { make: string, model: string, year: number };
myCar2 = {
    make: 'Honda',
    model: 'Accord',
    year: 1992,
    color: {r: 255, g: 0, b: 0}
} as { make: string, model: string, year: number };

// or
let myCar3: { make: string, model: string, year: number } = {
    make: 'Honda',
    model: 'Accord',
    year: 1992,
    color: {r: 255, g: 0, b: 0}
} as { make: string, model: string, year: number };

// My example of casting the object shape
let face: { nose: boolean, mouth: boolean, eyes: number }
face = {
    nose: true,
    mouth: true,
    eyes: 2,
    lips: true
} as { nose: boolean, mouth: boolean, eyes: number }

// Real life example:
// How to avoid repetitive like this:
let someCar: { make: string, model: string, year: number } = {
    make: 'Honda',
    model: 'Accord',
    year: 1992
};
let lisasCar: { make: string, model: string, year: number } = {
    make: 'Ford',
    model: 'Monster Truck',
    year: 2016
};
function carCageMatch(
    a: { make: string, model: string, year: number },
    b: { make: string, model: string, year: number }
) { /**/ }

// We define Interfaces
interface Car {
    make: string,
    model: string,
    year: number
};

let someCar2: Car = {
    make: 'Honda',
    model: 'Accord',
    year: 1992
};

let lisasCar2: Car = {
    make: 'Ford',
    model: 'Monster Truck',
    year: 2016
};

function carTest(car1: Car, car2: Car): object {
    return car1
}

// Interfaces
// -> Interfaces only describe structure, they have no implementation
// -> They don't compile to any JavaScript code.
// -> DRY type definition allows for easy refactoring later
// -> Interfaces are "open" and can be extended later on!

interface Car3 {
    make: string;
    model: string;
    year: number;
};

interface Car3 {
    color: string
}

let lisasCar: Car3 = {
    make: 'Ford',
    model: 'Monster Truck',
    year: 2016,
    color: '#fff'
};