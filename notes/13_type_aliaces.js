"use strict";
// Sometimes an interface isn't the best way to describe a structure
// We can use the type keyword to define a type alias
// You can export types and interfaces, so you can consume them in other modules!
var red = [255, 0, 0];
//# sourceMappingURL=13_type_aliaces.js.map